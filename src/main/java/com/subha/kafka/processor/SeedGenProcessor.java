package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by subha on 03/07/2018.
 */

@Component("seedGenerator")
public class SeedGenProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(SeedGenProcessor.class);

    private static int UPPER_BOUND = 99;
    private static int LOWER_BOUND = 1;
    private Random rand = new Random();

    public void process(Exchange exchange){
        int randomInteger = rand.nextInt(UPPER_BOUND-LOWER_BOUND)+LOWER_BOUND;
        LOG.info("Seed generator generated seed = " + randomInteger);
        exchange.getIn().setBody(randomInteger);
    }
}
