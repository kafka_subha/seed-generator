package com.subha.kafka.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by subha on 03/07/2018.
 */
@Component
public class SeedGenRouter extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:seedGen?level=ERROR&showHeaders=true&showBody=true")
                .useOriginalMessage());

        from("timer://producer?period={{message.interval}}")
                .routeId("seed-generator")
                .to("bean:seedGenerator?method=process")
                .to("kafka:{{kafka.topic}}?brokers={{kafka.server}}:{{kafka.port}}");

    }
}
