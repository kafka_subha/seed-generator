****Seed Generator Application****

This application will create a random seed between 1-99, and will send the message to 
a kafka topic. 

Topic configuration are defined in application.yml file. Also user can set a interval time
in the configuration. Application will create a message at given intervals.